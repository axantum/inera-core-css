import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Notifications",
  parameters: {
    status: "beta",
    docs: {
      description: {
        component: [""].join("\r\n"),
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    '<span class="ic-notification">0</span>',
    '<span class="ic-notification">2</span>',
    '<span class="ic-notification">7</span>',
    '<span class="ic-notification">99</span>',
    '<span class="ic-notification">9999</span>',
    '<span class="ic-notification"></span>',
  ]);

export const WithUtils = () =>
  markupWithCodeExample([
    '<span class="ic-notification iu-bg-cta">10</span>',
    '<span class="ic-notification iu-bg-muted">10</span>',
    '<span class="ic-notification iu-fs-xs">10</span>',
    '<span class="ic-notification iu-fs-md">10</span>',
    '<span class="ic-notification iu-bg-information-light iu-color-text">10</span>',
    '<span class="ic-notification iu-bg-success-light iu-color-text">10</span>',
  ]);
