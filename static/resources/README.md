Filerna i resources mappen används som resurser av komponentbibliotekets
demos på jsfiddle.

De inkluderas exempelvis med tjänsten githack.

https://bb.githack.com/ineraservices/inera-core-css/raw/develop/static/resources/css_theme_loader.js
