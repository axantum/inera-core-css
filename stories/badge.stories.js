import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Badge",
  parameters: {
    status: "wip",
  },
};

export const Default = () =>
  markupWithCodeExample([
    '<span class="ic-badge">This is a badge</span>',
    '<span class="ic-badge ic-badge--small">This is a badge</span>',
  ]);

export const Status = () =>
  markupWithCodeExample([
    '<span class="ic-badge-status">Unread</span>',
    '<span class="ic-badge-status ic-badge--small">Unread</span>',
    '<span class="ic-badge-status--alt">Unread</span>',
    '<span class="ic-badge-status--alt ic-badge--small">Unread</span>',
  ]);

export const Regional = () =>
  markupWithCodeExample([
    '<span class="ic-badge">Innehållet gäller Stockholms län</span>',
  ]);
