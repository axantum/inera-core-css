import json from "../../package.json";
const { version } = json;

export default `<div style="max-width: 66ch" class="ic-text">
  <h1>Användning och installation</h1>

  <p class="ic-preamble">

    Projektet är främst byggt för att inkluderas i en byggkedja. Ingen speciell
    hantering av exempelvis browser-prefix eller minifiering används utan detta
    är upp till dig som inkluderar projektet.
  </p>

  <p>Exempel på hur projektet kan
  byggas med gulp finns redan i projektet. Men det går lika bra att använda
  exempelvis webpack eller parcel.
  </p>


  <h2>Installation</h2>

  <p>Exemplen nedan är utifrån 1177 temat. Byt ut 1177 mot inera för att
  använda filerna för ineras tema.</p>

  <h3>Kopiera filer</h3>
  <p>
    I mappen <tt>/dist/</tt> finns färdiga CSS filer för varje tema som är
    skapat att kopiera exempelvis <tt>1177-master.css</tt> och
    <tt>inera-master.css</tt>
  </p>
  <p>
    Under <tt>/dist/min/</tt> finns även minifierade och versionstaggade css filer.
  </p>

  <p>Kopiera innehållet eller filen du vill använda till ditt projekt.</p>

  <h3>Inkludera från "cdn"</h3>
  <p>
    Du kan använda exempelvis
    <a href="https://raw.githack.com">githack.com</a> för att inkludera en
    master css i sidan under utveckling.
  </p>
  <p><strong>OBS!</strong> Ej lämpligt för produktion.</p>

  <h4>
    <a
      href="https://bb.githack.com/ineraservices/inera-core-css/raw/develop/dist/1177-master.css"
      >Develop</a
    >
  </h4>
  <p>
    <code
      >&lt;link rel="stylesheet"
      href="https://bb.githack.com/ineraservices/inera-core-css/raw/develop/dist/1177-master.css"
      /&gt;</code
    >
  </p>
  <h4>Senaste master</h4>
  <p>
    <a
      href="https://bbcdn.githack.com/ineraservices/inera-core-css/raw/v${version}/dist/1177-master.css"
    >
      https://bbcdn.githack.com/ineraservices/inera-core-css/raw/v${version}/dist/1177-master.css
    </a>
  </p>



  <h3>Användning med npm - package.json</h3>
  <p>
    För att lägga till repot i package.json klipp in denna rad i dina
    dependencies i package.json
  </p>

  <p>
    <code
      >"inera-core-css":
      "git+https://bitbucket.org/ineraservices/inera-core-css.git#master"</code
    >
  </p>

  <p>Och kör sedan kommandot: <code>npm install</code></p>

  <p>
    Du kan också ange branch, verstionstag eller commit sha i stället för
    #master exempelvis:
  </p>

  <p>
    <code
      >"inera-core-css":
      "git+https://bitbucket.org/ineraservices/inera-core-css.git#0.3.0"</code
    >
  </p>

  <p>
    <strong>OBS!</strong> När du installerar via package.json på detta sätt så
    kommer inte automatiskt nya versioner installeras med npm install eller npm
    update om modulen redan är installerad.
  </p>

  <p>
    <strong>Tips!</strong> Kombinera import från mapp i utveckling med
    package.json installation för bygge i produktion för att automatiskt få
    uppdateringar när du utvecklar.
  </p>

  <h2>Importera från mapp med webpack</h2>
  <p>
    Om du använder webpack kan du lägga in sökvägen under resolve
    konfigureringen. Exempel om du har en mapp som heter "gemensam-css" utanför
    projektets rotmap med en undermapp "inera-core-css" använd nedanstående
    konfigurering.
  </p>
  <pre><code>module.exports = {
  resolve: {
    modules: [
      'node_modules',
      path.resolve(__dirname, '../gemensam-css/'),
    ]
  }
}
</code></pre>
  <p>
    Ordningen ovan är viktig, men troligen vill du att 'node_modules' vinner,
    och då ska den vara först.
  </p>

  <p>Nu kan du importera en scss fil genom</p>

  <p><code>@import 'inera-core-css/src/themes/inera-master.scss';</code></p>

  <h3>Import från node_modules</h3>
  <p>
    Om du däremot har lagt in en referens i package.json git-URL då behövs det
    inte läggas till någon resolve-path, men istället skall importen ske genom:
  </p>

  <p><code>@import '~inera-core-css/src/themes/inera-master.scss';</code></p>

  <p>
    Anledningen till detta är att då vet webpack att det ska tas från
    node_modules, risken är annars att importen resolvas från din lokala klon av
    inera-core-css vilket kanske inte alls är samma version som refereras i din
    package.json.
  </p>

  <p>
    Mer om
    <a href="https://webpack.js.org/concepts/module-resolution/"
      >module resolution</a
    >
    i webpack.
  </p>

  <h2></h2>
</div>
`;
