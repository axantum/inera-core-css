import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Links",
  parameters: {
    status: "beta",
  },
};

export const Default = () =>
  markupWithCodeExample([
    `<a href="javascript:void(0)">Default link</a>`,
    `<a href="javascript:void(0)" class="ic-link">Link component</a>`,
    `<button type="button" class="ic-link">Link styled button component</button>`,
    `<a href="http://www.example.com" class="ic-link" target="_blank" rel="noopener">External link</a>`,
    `<a href="http://www.example.com" class="ic-link ic-link-external" target="_blank" rel="noopener">
  External link
  <span aria-hidden="true" class="icon-link-ext"></span>
</a>`,
  ]);

export const Versions = () =>
  markupWithCodeExample([
    '<a href="javascript:void(0)" class="ic-chevron-link">Link component with chevron</a>',
    '<a href="javascript:void(0)" class="ic-nav-link">A navigation link</a>',
  ]);
