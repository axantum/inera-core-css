import { storiesOf } from "@storybook/html";
import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

const ExpandableBlank = () =>
  markupWithCodeExample([
`<details class="ic-expandable">
  <summary class="ic-expandable-button iu-focus">Expandable section</summary>
  <p>Some hidden content</p>
  <p>This is a <a href="javascript:void(0)">a link</a></p>
  <p>Focus on the link should not be possible when closed.</p>
</details>`,

`<details class="ic-expandable" open>
  <summary class="ic-expandable-button iu-focus">Expandable section</summary>
  <p>Some hidden content</p>
  <p>This is a <a href="javascript:void(0)">a link</a></p>
  <p>Focus on the link should not be possible when closed.</p>
</details>`
  ]);

const ExpandableCard = () =>
  markupWithCodeExample([
`<details class="ic-card ic-card--expandable ic-card--sm-unset-style ic-expandable ic-card--inspiration-large">
  <summary class="ic-expandable-button ic-inner ic-expandable-button--chevron">Expandable section</summary>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
</details>`,   
    
`<details class="ic-card ic-card--expandable ic-card--sm-unset-style ic-expandable ic-card--inspiration-large" open>
  <summary class="ic-expandable-button ic-inner ic-expandable-button--chevron">Expandable section</summary>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
</details>`,
  ]);

const ExpandableSection = () =>
  markupWithCodeExample([
`<ul class="ic-expandables-group">
  <li class="">
    <details class="ic-expandable iu-color-cta">
      <summary class="ic-expandable-button ic-expandable-button--plusminus-circle">Expandable section</summary>
      <p>Hello world</p>
    </details>
  </li>
  <li class="">
    <details class="ic-expandable iu-color-cta" open>
      <summary class="ic-expandable-button ic-expandable-button--plusminus-circle">Expandable section</summary>
      <p>Hello world</p>
    </details>
  </li>
  <li class="">
    <details class="ic-expandable iu-color-cta">
      <summary class="ic-expandable-button ic-expandable-button--plusminus-circle">Expandable section</summary>
      <p>Hello world</p>
    </details>
  </li>
</ul>`,
]);

storiesOf("components/Expandables", module)
  .addDecorator(wrapStoryInContainer)
  .addParameters({
    docs: {
      description: {
        component: [''].join("\r\n"),
      },
    },
    status: "wip",
  })
  .add("Blank", () => {
    return ExpandableBlank();
  })
  .add("Card", () => {
    return ExpandableCard();
  })
  .add("Section", () => {
    return ExpandableSection();
  });
