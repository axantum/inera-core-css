export default `<div style="max-width: 66ch" class="ic-text">
  <h1>
    Välkommen till projektet<br />
    gemensam css
  </h1>

  <p class="ic-preamble">Detta är starten på en delad och gemensam CSS för inera.
  Här finns exempel på många olika komponenter du kan använda för att bygga
  upp sidor och layout.</p>

  <h2>Under uppbyggnad.</h2>

  <p>De flesta komponenter finns för både varumärket 1177 och Inera.</p>

  <p>
    Källkoden för inera-core-css finns
    <a href="https://bitbucket.org/ineraservices/inera-core-css/"
      >här på bitbucket</a
    >.
  </p>
  <h2>Fel och önskemål</h2>
  <p>
    Rapportera gärna fel eller önskemål under
    <a href="https://bitbucket.org/ineraservices/inera-core-css/issues/new"
      >issues</a
    >
    på bitbucket. Du behöver inloggning och rättigheter till bitbucket.
  </p>
</div>
`;
