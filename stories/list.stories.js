import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

export default {
  title: "components/Lists",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "beta",
    docs: {
      description: {
        component:
          "Lists can be used for example for search hits and news items.",
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<ul class="ic-block-list">
  <li class="ic-block-list__item">This is a block list</li>
  <li class="ic-block-list__item">This is a block list</li>
  <li class="ic-block-list__item">This is a block list</li>
</ul>
`,
    `
<ul class="ic-block-list ic-block-list--striped">
  <li class="ic-block-list__item">This is a block list</li>
  <li class="ic-block-list__item">This is a block list</li>
  <li class="ic-block-list__item">This is a block list</li>
</ul>
`,
  ]);

export const Linked = () =>
  markupWithCodeExample([
    `
<ul class="ic-block-list">
  <li class="ic-block-list__item ic-text ic-card--link">
    <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Heading</a></h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
  </li>
  <li class="ic-block-list__item ic-text ic-card--link">
    <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Heading</a></h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
  </li>
  <li class="ic-block-list__item ic-text ic-card--link">
    <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Heading</a></h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
  </li>
</ul>
`,
  ]);

export const Striped = () =>
  markupWithCodeExample([
    `
<ul class="ic-block-list ic-block-list--striped">
  <li class="ic-block-list__item ic-text ic-card--link">
    <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Heading</a></h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
  </li>
  <li class="ic-block-list__item ic-text ic-card--link">
    <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Heading</a></h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
  </li>
  <li class="ic-block-list__item ic-text ic-card--link">
    <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Heading</a></h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
  </li>
</ul>
`,
  ]);

export const NewsList = () =>
  markupWithCodeExample([
    `
<ul class="ic-block-list ic-block-list--striped">
  <li class="ic-block-list__item ic-card--link">

    <article class="ic-news-list-item">
      <time datetime="2019-08-30" class="ic-date-label ic-news-list-item__date">
        <span class="ic-date-label__day">30</span>
        <span class="ic-date-label__month">aug</span>
        <span class="ic-date-label__year">2019</span>
      </time>
      <div class="ic-news-list-item__body">
        <header>
          <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Heading</a></h3>
          <span class="ic-news-list-item__author">Av Inera</span>
        </header>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
        </p>
        <footer class="iu-mt-none">Publicerat: 2019-09-17   Område: Manualer och lathundar</footer>
      </div>
    </article>

  </li>
  <li class="ic-block-list__item ic-card--link">

    <article class="ic-news-list-item">
      <time datetime="2019-08-30" class="ic-date-label ic-news-list-item__date">
        <span class="ic-date-label__day">30</span>
        <span class="ic-date-label__month">aug</span>
        <span class="ic-date-label__year">2019</span>
      </time>
      <div class="ic-news-list-item__body">
        <header>
          <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Heading</a></h3>
          <span class="ic-news-list-item__author">Av Inera</span>
        </header>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
        <footer class="iu-mt-none">Publicerat: 2019-09-17   Område: Manualer och lathundar</footer>
      </div>
    </article>

  </li>
  <li class="ic-block-list__item ic-card--link">

    <article class="ic-news-list-item">
      <time datetime="2019-08-30" class="ic-date-label ic-news-list-item__date">
        <span class="ic-date-label__day">30</span>
        <span class="ic-date-label__month">aug</span>
        <span class="ic-date-label__year">2019</span>
      </time>
      <div class="ic-news-list-item__body">
        <header>
          <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Heading</a></h3>
          <span class="ic-news-list-item__author">Av Inera</span>
        </header>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
        <footer class="iu-mt-none">Publicerat: 2019-09-17   Område: Manualer och lathundar</footer>
      </div>
    </article>

  </li>
</ul>
`,
  ]);
