export default `
<div style="max-width: 66ch" class="ic-text">
  <h1>Utveckling</h1>
  <p class="ic-preamble">
    Vill du bidra till projektet eller sätta upp storybook
    som sandlåda för att testa komponenterna så finns information om hur du gör här.
  </p>
  <h2>Utveckling</h2>
  <p>
    För att köra utvecklingsmiljön behöver du node.js.
    <a href="http://nodejs.org/">Det kan du ladda hem och installera här</a>.
  </p>
  <p>
    Klona detta
    <a href="https://bitbucket.org/ineraservices/inera-core-css/src/develop/"
      >repository</a
    >. Gå till katalogen och kör
  </p>
  <p><code>$ npm install</code></p>
  <p>
    För att starta utvecklingsmiljön och kör följande kommando, öppna sedan
    http://localhost:6006/ webbläsaren.
  </p>
  <p><code>$ npm start</code></p>
  <p>För att skapa en färdig distributionsmapp kör.</p>
  <p><code>$ npm run build</code></p>
  <p>Detta kommer att rensa /dist/ mappen och skapa nya css filer.</p>

  <h2>Struktur</h2>
  <p>
    I mappen <tt>./src</tt> delar vi upp så att varje komponent har en egen fil
    under <tt>./src/components</tt>. Exempel button.css.
  </p>
  <p>
    I mappen <tt>./src/themes</tt> har varje tema en en variabel fil (1177.scss)
    och en master (1177-master.scss) som importerar allt samt default variabler
    (default.scss och default-master.scss).
  </p>
  <p>Under <tt>./src/utils</tt> finns hjälp funktioner och generella mixins.</p>

  <h3>Statiska filer</h3>
  <p>
    Lägg statiska filer (fonter etc.) som behövs för storybook under
    <tt>./static</tt> mappen. Dessa filer kommer servas från rooten när du kör
    storybook. Exempelvis <tt>./static/file.txt</tt> kan laddas från
    <tt>http://localhost:6006/file.txt</tt>
    när du kör storybook.
  </p>

  <h2>Versionshantering</h2>

  <p>
    Vi bör följa
    <a href="https://semver.org/lang/sv/">semantisk versionshantering</a>.
    Exempel på breaking changes: html-förändring, förändring av variabel-värde,
    implementation som kan påverka layout eller kringliggande element, eller
    förändring som på annat sätt påverkar bakåtkompabiliteten.
  </p>

  <h2>Några regler för utveckling</h2>

  <ul role="list">
    <li>
      Använd relativa enheter för storlekar. Det ska gå att skala upp typsnitt
      minst 200% och allt på sidan ska skala med.
    </li>

    <li>Undvik marginaler utåt mellan enskilda komponenter i biblioteket.</li>

    <li>
      Vid förändringar försök i första hand att inte behöva ändra HTML
      strukturen. Utan om möjligt få rätt resultat med CSS.
    </li>

    <li>Undvik implementeringar som kräver webbläsarspecifika prefix.</li>

    <li>Använd alltid semantisk och tillgänglig html.</li>

    <li>
      Se till så att en förändring av misstag inte påverkar ett annat tema.
    </li>

    <li>Undvik att nästla SCSS regler.</li>

    <li>Skriv CSS mobile-first.</li>

    <li>Undvik externa beroenden och bibliotek</li>

    <li>Föredra tydlighet och läsbarhet i koden du skriver.</li>
  </ul>

  <h3>Namngivning</h3>
  <h4>Inera Components och Inera Utilities</h4>
  <p>
    Vi använder prefixed <tt>.ic-</tt> för components och <tt>.iu-</tt> för
    utilities. Exempelvis <tt>.iu-radius-sm</tt> för liten border-radius eller
    .ic-button för en knapp.
  </p>
  <h4>BEM</h4>
  <p>
    Vi namnger klasser enligt BEM struktur. Exempelvis
    <tt>.c-button</tt> (Block), <tt>.c-button__icon</tt> (Element) och
    <tt>.c-button--primary</tt> (Modifier) för en knapp.
  </p>
  <p><a href="http://getbem.com/introduction/">Läs mer om BEM här.</a></p>

  <h4>Codestyle</h4>
  <p>
    Vi använder <a href="http://prettier.io/">prettier</a> för formatering av
    css/scss. D etta görs automatiskt när du gör en commit till detta
    repository. Detta för att ha ett enhetligt och objektivt regelsystem.
  </p>
  <p>
    Du kan förmodligen även installera Prettier till ditt IDE så att filerna
    formateras automatiskt när du sparar.
  </p>

  <h3>Temavariabler i SCSS</h3>
  <p>
    Vi har en del hjälpande variabler i scss filerna. Som hjälper till för mer
    konsekvent användning av exempelvis font-storlekar, färger och generella
    avståndsvärden.
  </p>
  <p>
    Generella regler kring dessa. Variablerna är skapade som listor i scss.
    Namnen kan då kommas åt med funktionen get. Exempel:
  </p>
  <code>get($fontSizes, 200)</code>
  <p>
    Listorna är definierade i default.scss temat och extendas från site-teman.
    Tanken är att värden "100" - "900" ska vara gemensamma från default. Medan
    namngivna variabler, som "md", "h1" eller "card" är temaspecifika.
  </p>

  <h2>Hjälpt till och bidra</h2>
  <p>
    Tillägg och förbättringar skickas som en pull request till detta repo som
    sedan godkänns och inkluderas. Tänk på att kolla igenom vilka komponenter
    som redan finns innan du skapar en ny.
  </p>
  <p>
    Kolla gärna under
    <a
      href="https://bitbucket.org/ineraservices/inera-core-css/issues?status=new&status=open"
      >issues</a
    >
    på bitbucket och se om någonting saknas eller kan förbättras.
  </p>
  <ul role="list">
    <li>Gör förändringar.</li>
    <li>Skapa en pull request.</li>
    <li>Uppdatera eventuella kodexempel.</li>
    <li>När pull request är godkänd gör en merge.</li>
  </ul>
  <p>Alla bidrag är välkomna!</p>
</div>
`;
