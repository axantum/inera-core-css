import { markupWithCodeExample, bleedContainer } from "./helpers";

export default {
  title: "components/Page Header",
  decorators: [bleedContainer],
  parameters: {
    status: "wip",
    docs: {
      description: {
        component:
          "More UX information on [Confluence](https://inera.atlassian.net/wiki/spaces/USI/pages/212674241/Ramverk+och+navigation+-+1177).",
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>
  </div>
</header>
`,
  ]);

export const LoggedIn = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>
    <div class="ic-page-header__item iu-mr-gutter ic-avatar-box">
      <div role="presentation" class="iu-svg-icon iu-mr-300 ic-avatar-box__icon">
        <!-- This SVG is for 1177 only -->
        <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
          <path fill="#6A0032" d="M24.719 10.5c0-3.524-3.106-6.344-7.219-6.344-3.364 0-6.344 2.935-6.344 6.344 0 3.87 3.232 8.094 6.344 8.094 3.847 0 7.219-4.062 7.219-8.094zm1.312 0c0 4.709-3.9 9.406-8.531 9.406-3.925 0-7.656-4.875-7.656-9.406 0-4.138 3.571-7.656 7.656-7.656 4.811 0 8.531 3.377 8.531 7.656z" transform="translate(-1058 -171) translate(0 140) translate(1058 31)"/>
          <g fill="#6A0032">
            <path d="M23.595 10.719c-.06-.194-.122-.398-.187-.62-.07-.234-.082-.277-.255-.879-.851-2.957-1.391-4.216-2.534-5.383-1.486-1.516-3.82-2.306-7.494-2.306-4.076 0-6.657.816-8.275 2.378C3.617 5.1 3.056 6.37 2.177 9.385l-.21.716c-.066.222-.127.426-.187.618h21.815zm-23.34.44c.146-.42.285-.86.454-1.431l.208-.71c.942-3.232 1.57-4.65 3.022-6.053C5.83 1.137 8.737.219 13.125.219c4 0 6.672.903 8.432 2.7 1.346 1.374 1.947 2.775 2.857 5.938l.253.872c.165.56.306 1.007.453 1.43.148.427-.169.872-.62.872H.875c-.451 0-.768-.445-.62-.871z" transform="translate(-1058 -171) translate(0 140) translate(1058 31) translate(5.25 21)"/>
            <path d="M20.117 10.955c.07.355-.16.7-.516.771-.355.07-.7-.16-.771-.516l-.666-3.353c-.07-.356.16-.701.516-.772.355-.07.7.16.771.516l.666 3.354zM5.515 7.601c.07-.355.416-.586.771-.516.356.071.586.416.516.772l-.666 3.353c-.07.356-.416.587-.771.516-.356-.07-.587-.416-.516-.771L5.515 7.6z" transform="translate(-1058 -171) translate(0 140) translate(1058 31) translate(5.25 21)"/>
          </g>
          <path fill="#C12143" d="M16.844 10.281H14c-.362 0-.656-.294-.656-.656 0-.362.294-.656.656-.656h3.5c.362 0 .656.294.656.656v3.719h1.094c.362 0 .656.294.656.656 0 .362-.294.656-.656.656H17.5c-.362 0-.656-.294-.656-.656v-3.719z" transform="translate(-1058 -171) translate(0 140) translate(1058 31)"/>
        </svg>
      </div>
      <div class="iu-lh-narrow">
        <p class="ic-avatar-box__name">Namn Namnsson Nilsson</p>
        <nav aria-label="Användarmeny">
          <ul>
            <li class="iu-hide-md iu-hide-sm iu-display-flex">
              <a href="javascript:void(0)" class="iu-link-divider-right iu-color-main">Inställningar</a>
            </li>
            <li class="iu-hide-md iu-hide-sm iu-display-flex">
              <a href="javascript:void(0)" class="iu-color-main">Logga ut</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <nav aria-label="Användarmeny mobil" class="ic-nav iu-fs-200 iu-hide-from-lg ic-nav--full ic-nav--divider iu-bg-secondary-light">
    <ul class="ic-container">
      <li>
        <a href="javascript:void(0)">Inställningar</a>
      </li>
      <li>
        <a href="javascript:void(0)">Logga ut</a>
      </li>
    </ul>
  </nav>
</header>
`,
  ]);

  export const LoggedInWithMobileMenu = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>
    <div class="ic-page-header__item iu-mr-gutter ic-avatar-box">
      <div role="presentation" class="iu-svg-icon iu-mr-300 ic-avatar-box__icon">
        <!-- This SVG is for 1177 only -->
        <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
          <path fill="#6A0032" d="M24.719 10.5c0-3.524-3.106-6.344-7.219-6.344-3.364 0-6.344 2.935-6.344 6.344 0 3.87 3.232 8.094 6.344 8.094 3.847 0 7.219-4.062 7.219-8.094zm1.312 0c0 4.709-3.9 9.406-8.531 9.406-3.925 0-7.656-4.875-7.656-9.406 0-4.138 3.571-7.656 7.656-7.656 4.811 0 8.531 3.377 8.531 7.656z" transform="translate(-1058 -171) translate(0 140) translate(1058 31)"/>
          <g fill="#6A0032">
            <path d="M23.595 10.719c-.06-.194-.122-.398-.187-.62-.07-.234-.082-.277-.255-.879-.851-2.957-1.391-4.216-2.534-5.383-1.486-1.516-3.82-2.306-7.494-2.306-4.076 0-6.657.816-8.275 2.378C3.617 5.1 3.056 6.37 2.177 9.385l-.21.716c-.066.222-.127.426-.187.618h21.815zm-23.34.44c.146-.42.285-.86.454-1.431l.208-.71c.942-3.232 1.57-4.65 3.022-6.053C5.83 1.137 8.737.219 13.125.219c4 0 6.672.903 8.432 2.7 1.346 1.374 1.947 2.775 2.857 5.938l.253.872c.165.56.306 1.007.453 1.43.148.427-.169.872-.62.872H.875c-.451 0-.768-.445-.62-.871z" transform="translate(-1058 -171) translate(0 140) translate(1058 31) translate(5.25 21)"/>
            <path d="M20.117 10.955c.07.355-.16.7-.516.771-.355.07-.7-.16-.771-.516l-.666-3.353c-.07-.356.16-.701.516-.772.355-.07.7.16.771.516l.666 3.354zM5.515 7.601c.07-.355.416-.586.771-.516.356.071.586.416.516.772l-.666 3.353c-.07.356-.416.587-.771.516-.356-.07-.587-.416-.516-.771L5.515 7.6z" transform="translate(-1058 -171) translate(0 140) translate(1058 31) translate(5.25 21)"/>
          </g>
          <path fill="#C12143" d="M16.844 10.281H14c-.362 0-.656-.294-.656-.656 0-.362.294-.656.656-.656h3.5c.362 0 .656.294.656.656v3.719h1.094c.362 0 .656.294.656.656 0 .362-.294.656-.656.656H17.5c-.362 0-.656-.294-.656-.656v-3.719z" transform="translate(-1058 -171) translate(0 140) translate(1058 31)"/>
        </svg>
      </div>
      <div class="iu-lh-narrow">
        <p class="ic-avatar-box__name">Namn Namnsson Nilsson</p>
        <nav aria-label="Användarmeny">
          <ul>
            <li class="iu-hide-md iu-hide-sm iu-display-flex">
              <a href="javascript:void(0)" class="iu-link-divider-right iu-color-main">Inställningar</a>
            </li>
            <li class="iu-hide-md iu-hide-sm iu-display-flex">
              <a href="javascript:void(0)" class="iu-color-main">Logga ut</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="ic-page-header__item iu-height-full">
      <!-- onclick only for demo -->
      <button
        onclick="this.querySelectorAll('.lines-button')[0].classList.toggle('close')"
        type="button"
        class="ic-burger ic-page-header__burger"
        aria-label="Öppna/stäng huvudmeny"
        aria-expanded="false"
      >
        <span class="lines-button x">
          <span class="lines"></span>
        </span>
      </button>
    </div>
  </div>
  <nav aria-label="Huvudmeny" class="ic-nav iu-fs-200 ic-nav--full ic-nav--divider iu-bg-secondary-light">
    <ul class="ic-container">
      <li>
        <a href="javascript:void(0)">Link</a>
      </li>
      <li>
        <a href="javascript:void(0)">Link</a>
      </li>
      <li>
        <a href="javascript:void(0)">Link</a>
      </li>
      <li>
        <a href="javascript:void(0)">Link</a>
      </li>
    </ul>
  </nav>
</header>
`,
  ]);

export const WithMenuUnder = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>
  </div>
  <nav aria-label="Huvudmeny" class="ic-topnav iu-hide-md iu-hide-sm">
    <ul class="ic-container">
      <li class="ic-topnav__item">
        <a class="ic-topnav__link selected" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-0">
          <span>Menu item 1</span>
        </a>
        <div class="ic-meganav" id="subnav-0" aria-hidden="true"></div>
      </li>
      <li class="ic-topnav__item">
        <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-1">
          <span>Menu item 2</span>
        </a>
        <div class="ic-meganav" id="subnav-1" aria-hidden="true"></div>
      </li>
      <li class="ic-topnav__item">
        <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-2">
          <span>Menu item 3</span>
        </a>
        <div class="ic-meganav" id="subnav-2" aria-hidden="true"></div>
      </li>
    </ul>
  </nav>
</header>
`,
  ]);

export const WithMenuSide = () =>
  markupWithCodeExample(`
<header class="ic-page-header">
  <div class="ic-page-header__inner">

    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>

    <nav aria-label="Huvudmeny" class="ic-page-header__item iu-align-self-end ic-topnav iu-hide-md iu-hide-sm">
      <ul class="ic-container">
        <li class="ic-topnav__item">
          <a class="ic-topnav__link selected" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-0">
            <span>Menu item 1</span>
          </a>
          <div class="ic-meganav" id="subnav-0" aria-hidden="true"></div>
        </li>
        <li class="ic-topnav__item">
          <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-1">
            <span>Menu item 2</span>
          </a>
          <div class="ic-meganav" id="subnav-1" aria-hidden="true"></div>
        </li>
        <li class="ic-topnav__item">
          <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-2">
            <span>Menu item 3</span>
          </a>
          <div class="ic-meganav" id="subnav-2" aria-hidden="true"></div>
        </li>
      </ul>
    </nav>

  </div>
</header>
`);

export const WithMobileMenu = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">

    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>

    <div class="ic-page-header__item iu-height-full">
      <!-- onclick only for demo -->
      <button
        onclick="this.querySelectorAll('.lines-button')[0].classList.toggle('close')"
        type="button"
        class="ic-burger ic-page-header__burger"
        aria-label="Öppna/stäng huvudmeny"
        aria-expanded="false"
      >
        <span class="lines-button x">
          <span class="lines"></span>
        </span>
      </button>
    </div>
  </div>

  <nav aria-label="Användareny" class="ic-nav-list" id="mobile-nav">
    <ul class="ic-nav-list__list">
      <li class="ic-nav-list__item">
        <a href="javascript:void(0)">Menu item 1</a>
        <button aria-expanded="false" aria-controls="submenu-0" class="ic-nav-list__expand">
          <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
        </button>
        <ul hidden class="ic-nav-list__list" id="submenu-0"></ul>
      </li>
      <li class="ic-nav-list__item">
        <a href="javascript:void(0)">Menu item 2</a>
      </li>
      <li class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded">
        <a href="javascript:void(0)">Menu item 3</a>
        <button aria-expanded="true" aria-controls="submenu-1" class="ic-nav-list__expand">
          <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
        </button>
        <ul class="ic-nav-list__list" id="submenu-1">
          <li class="ic-nav-list__item"><a href="javascript:void(0)">Submenu item 1</a></li>
          <li class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded">
            <a href="javascript:void(0)">Submenu item 2</a>
            <button aria-expanded="true" aria-controls="submenu-2" class="ic-nav-list__expand">
              <span class="iu-sr-only">Visa innehåll för Submenu item 2</span>
            </button>
            <ul class="ic-nav-list__list" id="submenu-2">
              <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub Submenu item 1</a></li>
              <li class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded">
                <a href="javascript:void(0)">Sub Submenu item 2</a>
                <button aria-expanded="true" aria-controls="submenu-3" class="ic-nav-list__expand">
                  <span class="iu-sr-only">Visa innehåll för Sub Submenu item 2</span>
                </button>
                <ul class="ic-nav-list__list" id="submenu-3">
                  <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub sub submenu item 1</a></li>
                  <li class="ic-nav-list__item  ic-nav-list__item--has-children ic-nav-list__item--expanded">
                    <a href="javascript:void(0)">Sub sub submenu item 2</a>
                    <button aria-expanded="true" aria-controls="submenu-4" class="ic-nav-list__expand">
                      <span class="iu-sr-only">Visa innehåll för Sub Submenu item 2</span>
                    </button>
                    <ul class="ic-nav-list__list" id="submenu-4">
                      <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub sub sub submenu item 1</a></li>
                      <li class="ic-nav-list__item"><a aria-current="page" href="javascript:void(0)">Sub sub sub submenu item 2</a></li>
                      <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub sub sub submenu item 3</a></li>
                      <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub sub sub submenu item 4</a></li>
                    </ul>
                  </li>
                  <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub sub submenu item 3</a></li>
                  <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub sub submenu item 4</a></li>
                </ul>
              </li>
              <li class="ic-nav-list__item"><a href="javascript:void(0)">Submenu item 3</a></li>
            </ul>
          </li>
          <li class="ic-nav-list__item"><a href="javascript:void(0)">Submenu item 3</a></li>
        </ul>
      </li>
      <li class="ic-nav-list__item">
        <a href="javascript:void(0)">Menu item 4</a>
      </li>
      <li class="ic-nav-list__item">
        <a href="javascript:void(0)">Menu item 5</a>
      </li>
    </ul>
  </nav>

</header>
`,
  ]);

export const WithMobileTools = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>
  </div>
  <nav aria-label="Huvudmeny" class="ic-nav iu-fs-200 ic-nav--full ic-nav--divider iu-bg-secondary-light">
    <ul class="ic-container">
      <li>
        <a href="javascript:void(0)">Link</a>
      </li>
      <li>
        <a href="javascript:void(0)">Link</a>
      </li>
      <li>
        <a href="javascript:void(0)">Link</a>
      </li>
      <li>
        <a href="javascript:void(0)">Link</a>
      </li>
    </ul>
  </nav>
</header>
`,
  ]);
