const Prism = require("prismjs");

/**
 * Take a callback and array and map the array
 * with the callback and then join the result with empty string.
 *
 * Example:
 * ```
 * const wrapInDiv = (html) => `<div>${html}</div>`;
 * htmlFromArray(wrapInDiv, [`<p>test</p>`, `<p>test2</p>`]);
 * // result: "<div><p>test</p></div><div><p>test</p></div>"
 * ```
 *
 */
export function htmlFromArray(cb = () => {}, array = []) {
  return array.map(cb).join("");
}

/**
 * Convert &, <, >, " to html enteties.
 * ```
 * // & -> &amp;
 * // < -> &lt;
 * // > -> &gt;
 * // " -> &quot;
 * ```
 */
export function htmlEntities(str) {
  return String(str)
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;");
}

/**
 * Take html-string or array with html-strings
 * and wrap in div.sb-demo and add code example
 * highlighted with Prism in code.sb-demo
 */
export function markupWithCodeExample(html) {
  let arr = typeof html === "string" ? [html] : html;
  return htmlFromArray(
    (item) =>
      `<div class="sb-demo">${item}</div><details class="sb-details" open><summary class="sb-summary">source</summary><pre class="sb-demo language-html"><button class="sb-unstyled-button" aria-label="focus on me"></button>${Prism.highlight(
        item.trim(),
        Prism.languages.html,
        "html"
      )}</pre></details>`,
    arr
  );
}

/**
 * Storybook Decorator
 *
 * Wrap a story in a div with classname,
 * default to `sb-container`;
 */
export function wrapStoryInContainer(story, className) {
  const tale = story();
  if (!className || typeof className !== "string") {
    className = "sb-container";
  }
  if (typeof tale === "string") {
    return '<div class="' + className + '">' + tale + "</div>";
  }
}

/**
 * Storybook Decorator
 *
 * Wrap a story in a div with
 * classname `sb-container-bleed`.
 */
export function bleedContainer(story) {
  return wrapStoryInContainer(story, "sb-container-bleed");
}
